from django.urls import path

from . import views


app_name = 'users'

urlpatterns = [
    path('', views.index, name='index'),
    path('get_bill/', views.get_bill, name='get_bill'),
    path('<int:user_id>/', views.save_user, name='save_user'),
    path('update_trent/', views.update_trent, name='update_trent'),
    path('delete_trent/', views.delete_trent, name='delete_trent'),
    path('check_from/', views.check_user_form, name='check_user_form'),
    path('check_user/', views.check_user, name='check_user'),
    path('add_form/', views.add_user_form, name='add_user_form'),
    path('add_user/', views.add_user, name='add_user'),
    path('add_user_image/', views.add_user_image, name='add_user_image'),
    path('upload_handle/', views.upload_handle, name='upload_handle'),
    path('show_avatar/', views.show_avatar, name='show_avatar'),
]