from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render

from utils.uploads import getNewName
from .models import User
from django.conf import settings
import os

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def get_bill(request):
    # 获取所有用户
    # all_users = User.objects.all()

    # 获取有条件的获取用户
    user = User.objects.filter(name='bill')

    return HttpResponse(user[0].phone)


def save_user(request, user_id):
    user = User()
    user.id = user_id
    user.name = 'trent'
    user.phone = '13888888888'
    user.password = '2345'

    # 调用save则保存数据到数据库中
    user.save()
    return HttpResponse('储存这个user成功')


def update_trent(request):
    User.objects.filter(name='trent').update(phone='12388888888', password='3456')
    return HttpResponse('更新trent的信息成功')


def delete_trent(request):
    User.objects.filter(name='trent').delete()

    return HttpResponse('删除trent成功')

def check_user_form(request):
    return render(request, 'users/check_user_form.html')

def check_user(request):
    request.encoding = 'utf-8'
    if 'username' in request.GET:
        user = get_object_or_404(User, name = request.GET['username'])
        user_info = {'user_id':user.id, 'user_name':user.name, 'user_phone':user.phone, 'user_password':user.password}
    return render(request, 'users/show_user.html', {'user_info':user_info})

def add_user_form(request):
    return render(request, 'users/add_user_form.html')

def add_user(request):
    if request.method=='GET':
        return render(request,'users/add_user_form.html')
    else:
        username=request.POST.get('username','')
        phone = request.POST.get('phone', '')
        password=request.POST.get('password','')

        user = User()
        user.name = username
        user.phone = phone
        user.password = password
        user.save()

        return HttpResponse('username='+username+"&userphone="+phone+"&password="+password+" 保存成功")

def add_user_image(request):
    return render(request, 'users/add_user_image.html')

def upload_handle(request):
    # 获取一个文件管理器对象
    file = request.FILES['pic']

    # 保存文件
    new_name = getNewName('avatar')

    where = '%s/users/%s' % (settings.MEDIA_ROOT, new_name)
    content = file.chunks()
    with open(where, 'wb') as f:
        for i in content:
            f.write(i)

    # 保存文件上传的记录
    User.objects.filter(name='trent').update(avatar=new_name)
    # 返回的httpresponse
    return HttpResponse('ok')

def show_avatar(request):
    user = User.objects.filter(name='trent')[0]
    avatarName = str(user.avatar)
    # avatarUrl = '%s/users/%s' % (settings.MEDIA_URL, avatarName) # 另一种写法
    avatarUrl = os.path.join(settings.MEDIA_URL, 'users', avatarName)
    avatar_info = {'userName':'trent', 'avatarUrl': avatarUrl}
    return render(request, 'users/show_avatar.html', {'avatar_info':avatar_info})