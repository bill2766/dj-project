from django.db import models

# Create your models here.
class User(models.Model):
    """
    创建用户表
    """
    name = models.CharField(max_length=128, null=True, unique=True, verbose_name='昵称')
    phone = models.CharField(max_length=128, null=True, unique=True, verbose_name='手机号')
    password = models.CharField(max_length=128, null=True, unique=True, verbose_name='密码')
    avatar = models.ImageField(upload_to='users', max_length=100, blank=True, null=True, verbose_name='用户头像')

    # meta类是配合admin进行管理的
    class Meta:
        db_table = 'web_user'
        ordering = ['name']  # 默认正序，倒序的话是'-name'
        verbose_name = '用户'
        verbose_name_plural = verbose_name